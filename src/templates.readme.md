Templates: Translator's Guide
---

We use `html` templates for emails, and this shows how translators can provide content for them.


### Locations
----

The template definitions are under `/template` but translators concern themselves only with their text content.

The text content for templates goes in the usual `{{lng}}_translation.json` files, alongside all the other content for the user interface.

Templates keys must be placed under:

```json
...

"template": {

    "some_template": {... },
    "some_other_template": {...}
    ...
}
...
```

> actual template names are documented below.


### Structure
----------

All templates have the same logical structure. These are its parts:

- the `subject`.
  
- a `main` title, which shows with most prominance within the email.

- an `explainer`, which renders below `main` in the role of a subtitle or explanatory (short) paragraph.

- an `action` that renders below the `explainer` as a button and typically links to relevant views into the app. 

- a `blurb`, which renders below the `action` to provide lengthier, possibly multi-paragraph content.

- a `context` which is rendered above the title to capture the broad scope of the email, header-like, so that it doesn't have to be mentioned in the content below.

> all parts are optional, except for `main` and `action`, which are mandatory.

> to date, we use the `context` to indicate and link to the campaign that scopes some of the mails. 

> There are other parts to the template, such as the header and the footer. But we currently don't allow per-mail customisations to their content, which is boilerplate.

### Keys
----------

As usual, translators provide content for each part in specific keys, but each template define the specific shape of keys. 

For example, the `subchange` template below expects the `main` content at:

 eg.:

```
assessed.requirement.notenant.main
```

which is used for the mail sent to users with `notenant` (IOTC) when `requirement` submissions are `assessed`.

In fact, each template defines multiple keys for each part, so that translators can customise content based on contextual information.

For example, the `subchange` template will pick this key:

```
assessed.product.tenant.main
```

if the mail goes to users a `tenant` when `product` submissions are `assessed`.

or even this one:

```
assessed.default.main
```

if the `main` content of the mails above should be the same across assets and users.

In general, for each part, a template will define a _chain_ of keys that it will consider when looking for content. 

Specifically, it will consider each key in turn an use the very first it finds defined.

Chains start with the most specific keys - the content is specific to all pieces of context - and progressively generilise towards least generic keys - the content is the same for all pieces of context.

> key chains for each template are defined below.


### Parameters
--------------

As usual, translators can use parameters to customise the content at each key.

Each template introduces its own parameters, but _all_ templates recognise a small number of _common parameteras_, like:

- `tenant`: the name of CPC the mail is about.
- `campaign`: the name of the campaign the mail is about.
- `asset`: the name of the requirement or product the mail is about.
  
> The names will be resolved in language of the mail recipient.

> Even though common parameters are recognised by all templates, they may have no value in some template. For example, the `duedate` template is about an event, not a `tenant`, so `tenant` won't resolve to any content.

The following common parameters resolve to the their counterpart above, only as links (wrapped in anchors)

- `tenant_link`
- `campaign_link`
- `asset_link`

> The actual links change from recipient to recipient. `CPC`s are sent to the `RC`, `IOTC` users to the `MC`. Links change also across `CPC` s. 

Additional "link parameters" include:

- `app_link`: links to the application as a whole with the name `e-MARIS`.
- `submission`: an alias for `asset`.
- `submission_link`: an alias for `asset_link`

> the `app_link` is used mostly by the template to provide boilerplate content in the footer of the email, but it may be used in other parts of the email too.

> `submission` and `submission_link` are just helper to contextualise furthere the content of the email. Future mail templates, may prefer to use `asset` and `asset_link` to refer to requirement and product templates themselves, rather than submissions.

There are also a host of raw "url parameters", iuncluding:

- `app_url`
- `campaign_url`
- `tenant_url`
- `asset_url`
- `submission_url`
- `action_url`.

thouhg these are used primarily to form links. They are nonetheless exposed to translators in case they want to build lower-level content (e.g. in blurbs), and manually embed `html` codes in it.

> `action_url`: defaults to the `submission_url` if available, with fallbacks to `campaign_url` and `app_url`.


## Template: Submission Change
------------------------------

This is the template for emails sent to users when submissions transition to some relevant state in their lifecycle.

- name: `subchange`

The key chain for the template is:

- `{{status}}.{{assetType}}.{{tenancy}}.{{part}}`
- `{{status}}.{{assetType}}.{{part}}` 
- `{{status}}.default.{{tenancy}}.{{part}}`
- `{{status}}.default.{{part}}`
- `default.{{tenancy}}.{{part}}`
- `default.{{part}}`

where `status` can be:

- `pending`: submission has been submitted for internal approval.
- `requestforchange`: submission has to be changed before being submittted again for internal approval.
- `submitted`: submission has been submitted for assessment (maybe after approval).`
- `assessed`: the submission has been asssed.
- `revokedassessment`: the assessment has been revoked.
- `shared`: the submission has been shared.

and `assetType` can be:

- `requirement`
- `product`

and `tenancy` can be;

- `tenant`: the content is for `CPC` recipients.
- `noTenant`: the content is for `IOTC` recipients.

and `part` can be:

- `context`, `main`, `explainer`, `action`, `blurb`


> this template doesn't introduce specific parameters.


## Template: Due Date
------------------------------

This is the template for emails sent to users when campaign events do occur, including reminders of other events.

- name: `duedate`

The primary key chain for the template is:

- `{{assetType}}.{{eventId}}.{{part}}`
- `{{assetType}.default.{{part}}`
- `default.{{part}}`
- 
where `assetType` can be:

- `requirement`
- `product`

and `eventId` is the internal identifier of the event.

> using the `eventId` means translators can provide content for custom events (such as meetings).

The tmeplate supports an extended key chain for **reminders**:

- `EV-event-reminder.{{assetType}}.{{eventId}}.{{part}`
- `EV-event-reminder.{{assetType}}.default.{{part}}`
- `EV-event-reminder.default.{{part}}`
- `default.{{part}`

`EV-event-reminder` is the internal identifier of the reminder event. 

> Now and for the foreseable future, this is the only type of *temporal event* -- i.e. event about other events -- that we need. 

> If we do introduce other temporals -- e.g. an anniversary, or "past reminder" -- then `EV-event-reminder` can be replaced with their identifiers. So the general chain for temporal is:

> - `{{temporalId}}.{{assetType}}.{{eventId}}.{{part}}`
> - `{{temporalId}}.{{assetType}}.default.{{part}}`
> - `{{temporalId}}.default.{{part}}`
> - `default.{{part}}`

This template introduces the following parameters:

- `date`: the absolute date of the event, in a medium-length format.
- `date_short`: the absolute date of the event, in a short format.
- `date_long`: the absolute date of the event, in a long format.