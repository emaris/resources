[#setting url_escaping_charset="UTF-8"]
Hi ${user.firstName}, you have a brand new account.

To access the system, click on the following link and setup a password:
  [#assign domain = "localhost" /]
  [#assign auth_base_url = "https://${domain}:9000" /]
  [#assign app_base_url = "http://${domain}:3000" /]
  
${auth_base_url}/password/change/${changePasswordId}?tenantId=${user.tenantId}

This is an automated email, please do not reply directly to it.