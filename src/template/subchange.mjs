import { augment, prefixed } from './common.mjs'
import { template } from './template.mjs'

export const subject =  (t, params = {}, original) => {

    const [prefixedT,duedateparams] = preprocess(t,params,original)

    return prefixedT('subject',duedateparams)

}


export const render = (t, params = {}, original={}) => {

    const [prefixedT,duedateparams] = preprocess(t,params,original)

    return template(prefixedT,duedateparams)

}

const preprocess = (t, params = {}, original={}) => {

    const { assetType, status, targetTenant } = params

    const subchangeparams = augment({ 
        ...params, 
        request:original,
        observation: params.observation || t('template.subchange.assessed.observation.fallback')
    })

    const baseKey = `template.subchange`

    const tenancy = targetTenant ? "tenant" : "notenant"

    const chain = [
        `${baseKey}.${status}.${assetType}.${tenancy}}`, 
        `${baseKey}.${status}.${assetType}`, 
        `${baseKey}.${status}.default.${tenancy}`,
        `${baseKey}.${status}.default`,
        `${baseKey}.default.${tenancy}`,
        `${baseKey}.default`
    ]
    
    const prefixedT = prefixed(t,subchangeparams,chain)

    return [prefixedT,subchangeparams]

}