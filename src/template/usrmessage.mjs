import { augment, prefixed } from './common.mjs'
import { template } from './template.mjs'

export const subject = (t, params = {}, original) => {

    const [prefixedT, usrmsgparams] = preprocess(t, params, original)

    // resolves a specific subject, or the main content as a fallback.
    const subject = prefixedT(['subject', 'main'], usrmsgparams)

    return params.reply ? `Re: ${subject}` : subject

}

export const render = (t, params = {}, original = {}) => {

    const [prefixedT, usrmsgparams] = preprocess(t, params, original)

    return template(prefixedT, usrmsgparams)

}

const preprocess = (t, params = {}, original = {}) => {

    const { content, asset, targetTenant, author, authorTenant, tenant, reply } = params

    const usrmsgparams = augment({
        ...params,
        request: original
    })

    usrmsgparams.action_url = usrmsgparams.action_url === usrmsgparams.submission_url  ? `${usrmsgparams.action_url}?tab=message` : `${usrmsgparams.action_url}/message`

    const baseKey = `template.usrmessage`

    const tenancy = targetTenant ? "tenant" : "notenant"

    const chain = [
       `${baseKey}.default.${tenancy}`,
        `${baseKey}.default`
    ]


    if (asset)
        chain.push(
            `${baseKey}.default.asset.default.from_${authorTenant}`,
            `${baseKey}.default.asset.${tenancy}`,
            `${baseKey}.default.asset`
        )

    else if (tenant)
        chain.push(
            `${baseKey}.default.party.default.from_${authorTenant}`,
            `${baseKey}.default.party.${tenancy}`,
            `${baseKey}.default.party`
        )

    else chain.push(
        `${baseKey}.default.campaign.default.from_${authorTenant}`,
        `${baseKey}.default.campaign.${tenancy}`,
        `${baseKey}.default.campaign`
    )

    const prefixedT = prefixed(t, usrmsgparams, chain)

    //console.log({author, reply, authorTenant, result: prefixedT(`sender_*}`, author)})

    // use sender if available, or: 1) for CPC, translate the name of the No Tenant, 2) for No Tenant, use the name of the tenant.
    const signature = tenancy === "tenant" ? prefixedT(`sender_${authorTenant}`, author) : author

    let text = content

    if (reply)
        text = `<div align="center"><span class="left-border" style="background:teal;color:teal;">.</span><span class="left-border" style="background:white;color:white;width:0px;">_.</span><span style="text-align:left; padding-left:20px;font-size:smaller; font-style:italic;">${reply}</span></div><div align="center" style="margin-top:20px">${content}</div>`

    usrmsgparams.explainer = `<div align="center" style="display: inline-block; text-align:left; max-width:75%; padding-top:30px;">${text}<div align="center" style="margin-top:15px;font-size:smaller; font-style:italic">${signature}</div></div>`

    return [prefixedT, usrmsgparams]

}
