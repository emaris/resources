import { augment, prefixed } from './common.mjs'
import { template } from './template.mjs'

export const render = (t, params = {}, original={}) => {

    const personaltestparams = augment({ ...params, request:original })

    const baseKey = `template.personaltest`

    const prefixedT = prefixed(t,personaltestparams, [baseKey])

    return template(prefixedT,personaltestparams)

}
