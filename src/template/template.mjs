export const template = (t, clientparams) => {

    const {campaign, tenant, targetTenant} = clientparams

    // by default, context for Party is campaign, for No Tenant is Campaign & Party
    const defaultContext = () =>  `${campaign ? 
      
        (targetTenant ? 
            `<span class='context-pill' align="center">${campaign}</span>` 
          : 
            `<div class='context-campaign' align="center">${campaign}</div>${(tenant) ? `<span class='context-pill'>${tenant}</span>` : ''}`
        )
        : ''}`
    
    const params = { context: t('context',defaultContext()), 
                     main: t('main','{{main?}}'), 
                     explainer: t('explainer'), 
                     action: t('action'), 
                     blurb: t('blurb'), 
                     header: t('header'),
                     footer: t('footer'),
                     ...clientparams 
                    }

    return `
<!DOCTYPE html>
<html>
    <head>
    <meta name="color-scheme" content="light">
    <style type="text/css">
        * {
            text-align: center;
            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'PingFang SC', 'Ubuntu', 'Hiragino Sans GB', 'Microsoft YaHei', 'Helvetica Neue', Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol' !important;
        }
        body {
            height: 100% !important;
            margin: 0px;
            box-sizing: border-box;
            color: darkslategray !important;
            background: rgb(46, 69, 82) !important;
        }
        .logo {
            margin: 10px;
        }
        .logo img {
            margin: 0 auto;
            display: block;
        }
        .logo-text {
            font-size: smaller !important;
            color: lightblue !important;
        }
        .hf {
            padding: 15px;
            background: rgb(46, 69, 82) !important;
        }
        .hf .footer {
            padding: 12px 0;
            white-space: nowrap !important;
        }
        .hf .footer div {
            margin: 3px;
        }
        .asset {
            font-weight: 200 !important;
        }
        .hf-content {
            font-size: 12px !important;
            white-space: pre-line;
            color: lightslategray !important;
        }
        .main {
            text-align: center;
            font-size: 32px !important;
            margin-bottom: 0px;
            font-weight: 200 !important;
            white-space:pre-wrap;
            color: darkslategray !important;
        }
        .explainer {
            text-align: center;
            font-size: 20px !important;
            font-weight: 200 !important;
            margin-top: 8px;
            color: lightslategrey !important;
            white-space:pre-wrap;
        }
        .context {
            margin-top: 10px;
            margin-bottom: -10px;
        }
        .context-campaign {
            font-size: 16px !important;
            color: lightseagreen !important;
        }
        .main {
            margin-top: 40px;
        }
        .header {
            height: 42px;
        }
        .context-pill {
            display: inline-block;
            font-size: 16px !important;
            text-transform: lowercase !important;
            font-variant: small-caps !important;
            padding: 3px 8px;
            background: rgb(234, 238, 237);
            border-radius: 6px;
            color: darkslategray !important;
        }
        .context-campaign .context-tenant {
            margin-top:8px;
        }
        .blurb {
            margin-top:20px;
            padding: 50px 100px;
            font-weight: 300 !important;
            text-align: center;
            white-space: pre-wrap;
            color: darkslategray !important;
        }
        a {
            text-decoration: none;
            color: inherit;
            color: darkcyan !important;
        }
        .emphasis {
            font-weight: 300 !important;
        }
        .action {
            margin-top: 40px;
            padding: 10px 15px;
            border-radius: 4px;
            color: white !important;
            background: lightseagreen !important;
            display: inline-block;
        }
        .action:hover {
            background: #1ca8a1 !important;
            cursor: pointer;
        }
        .left-border {
            width: 3px;
            overflow: hidden;
            display: inline-block;
            margin-bottom: -5px;
          }
      </style>
      <!--[if mso]>
          <style>
              .context {
                  margin-top: 35px;
              }
              .left-border {
                margin-bottom: 0px !important;
              }
          </style>
      <![endif]-->
    </head>
    <body>

    <div class="hf">
        <div class="logo" align="center">
        <img width="120" src="${process.env.APP_URL}/mc/images/mclogo.png">
        ${params.header ? `<div class='hf-content header'>${params.header}</div>`: ''}
        </div>
    </div>

    <!--[if mso]>
    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="background: white;">
    <tr>
    <td width="100%" valign="top" height="500">
    <![endif]-->
      <div style="padding: 40px 20px !important; background: white !important; min-height: 500px !important;">
        ${params.context ? `<div class="context" align="center">${params.context}</div>` :''}
        <div class="main" align="center">${params.main}</div>
        ${params.explainer ? `<div class="explainer">${params.explainer}</div>` :''}
        <div align="center" style="margin-top:50px;margin-bottom:30px;">
          <!--[if mso]>
          <table role="presentation" cellspacing="0" cellpadding="0" border="0">
          <tr>
          <td class="action">
          <![endif]-->
            <a class="action" href="${params.action_url}">${params.action}</a>
          <!--[if mso]>
          </td>
          </tr>
          </table>
          <![endif]-->
        </div>
        ${params.blurb ? `<div class="blurb">${params.blurb}</div>` : ''}
      </div>
    <!--[if mso]>
    </td>
    </tr>
    </table>
    <![endif]-->

    <!--[if mso]>
    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tr>
    <td width="100%" valign="middle" style="padding:30px 0px">
    <![endif]-->
      <div class="hf" style="padding:30px 0px" align="center">
        <div class='hf-content footer'>
          ${params.footer}
        </div>
      </div>
    <!--[if mso]>
    </td>
    </tr>
    </table>
    <![endif]-->    
  </body>
</html>
`}
