import { augment, prefixed } from './common.mjs'
import { template } from './template.mjs'


export const subject = (t, params = {}, original) => {

    const [prefixedT, duedateparams] = preprocess(t, params, original)

    return prefixedT(['subject', 'main'], duedateparams)

}


export const render = (t, params = {}, original = {}) => {

    const [prefixedT, duedateparams] = preprocess(t, params, original)

    return template(prefixedT, duedateparams)

}

const preprocess = (t, params = {}, original = {}) => {

    const { lng, eventId, event, eventDate, temporal, assetType } = params

    const date = eventDate ? parseInt(eventDate) : Date.now()

    const dateModifier = Date.now() > date ? 'after' : 'before'

    const duedateparams = augment({

        ...params,

        request: original,

        event: `<span class="emphasis">${event}</span>`,

        date: `<span class="emphasis">${new Intl.DateTimeFormat(lng, { dateStyle: 'medium' }).format(date)}</span>`,

        date_long: `<span class="emphasis">${new Intl.DateTimeFormat(lng, { dateStyle: 'full' }).format(date)}</span>`,

        date_short: `<span class="emphasis">${new Intl.DateTimeFormat(lng).format(date)}</span>`,

    })

    const baseKey = `template.duedate`

    const baseTemporalKey = `template.duedate.${temporal}`
    const baseTemporalModifierKey = dateModifier === 'before' ? `${baseTemporalKey}.before` : `${baseTemporalKey}.after`


    const baseKechain = [
        `${baseKey}.${assetType}.${eventId}`,
        `${baseKey}.${assetType}.default`,
        `${baseKey}.default`]


    const temporalKeychain = eventDate ?

        [
            `${baseTemporalModifierKey}.${assetType}.${eventId}`,
            `${baseTemporalKey}.default.${assetType}.${eventId}`,
            `${baseTemporalModifierKey}.${assetType}.default`,
            `${baseTemporalKey}.default.${assetType}.default`,
            `${baseTemporalModifierKey}.default`,
            `${baseTemporalKey}.default`
        ] :

        [

            `${baseTemporalKey}.nodate`
        ]

    const keychain = temporal ? [...temporalKeychain, ...baseKechain] : baseKechain

    const prefixedT = prefixed(t, duedateparams, keychain)

    return [prefixedT, duedateparams]

}