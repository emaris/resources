import { augment } from './common.mjs'

export const render = async (t, params = {}, original = {}) => {

    const { template, campaign, tenant, targetTenant } = params

    if (!template)
        throw new Error("no template key for subject template.")

    // import template to get a custom subject renderer.
    const { subject: subjectRenderer } = await import(`./${template}.mjs`)

    let subject = subjectRenderer?.(t,params,original)

    // fallback to default subject renderer that translates conventional key.
    if (!subject) {

        const subjectparams = augment({ ...params, request: original })
        return t(`template.${template}.subject`, subjectparams)

    }

    const contextualised = targetTenant ? `${campaign}: ${subject}` : tenant ? `${campaign} - ${tenant}: ${subject}` : `${campaign}: ${subject}`

    return  contextualised.replace(/<[^>]*>?/gm, '')
}

  