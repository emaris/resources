import process from 'process'



export const linksFrom = (params = {}) => {

    const { targetTenant, targetTenantId, tenant, tenantId, campaign, campaignId, asset, assetId, assetTitle: assetLong, assetType, request: r } = params

    // if the event is about a tenant, we use its id to build links.
    // if the event is NOT about a tenant, but the recipient IS a tenant, we use the recipient id to build links. 
    const partyId = tenantId || targetTenantId 

    const assetTitle =  assetLong || asset

    const app_url = `${process.env.APP_URL}/${targetTenant ? 'rc' : 'mc'}`
    const app_link = anchorOf(app_url, 'e-MARIS')

    const campaignEmphasis =  campaign ? `<span class='emphasis'>${campaign}</span>` : campaign
    
    const campaign_url = campaignId &&  (partyId ?
        `${app_url}/dashboard/${campaignId}/tenant/${partyId}` :
        `${app_url}/dashboard/${campaignId}`)

    const campaign_link = anchorOf(campaign_url, campaignEmphasis)

    const asset_url = campaignId && assetId && `${app_url}/dashboard/${campaignId}/${assetType}/${assetId}`
    const asset_link = anchorOf(asset_url, asset)
    const asset_long_link = anchorOf(asset_url, assetTitle)

    const party_url = campaignId && partyId && `${app_url}/dashboard/${campaignId}/tenant/${partyId}`
    const party_link = anchorOf(party_url, partyId)

    const submission_url = partyId && campaignId && assetId && `${app_url}/dashboard/${campaignId}/tenant/${partyId}/${assetType}/${assetId}`
    const submission_link = anchorOf(submission_url, asset)

    const action_url = submission_url ?? (targetTenant ? party_url : asset_url) ?? party_url ?? campaign_url ?? app_url

    const assetStyled = `<span class='emphasis'>${asset}</span>`
    const assetTitleStyled = `<span class='emphasis'>${assetTitle}</span>`

    return Object.entries({

        campaign: campaignEmphasis,
       
        app_url,
        app_link,

        action_url,

        campaign_url,
        campaign_link,
        tenant_link: anchorOf(campaign_url, tenant),

        asset: assetStyled,
        asset_url,
        asset_link,

        asset_long: assetTitleStyled,
        asset_long_link,

        submission: assetStyled,
        submission_url,
        submission_link,

        party_url,
        party_link,

        submission_long: assetTitleStyled,
        submission_long_link: asset_long_link,

    }).reduce((acc, [key, val]) => ({ ...acc, [key]: val || (key.includes('_url') ? app_url : key.includes('_link') ? app_link : val) }), {})

}

export const anchorOf = (href, text) => href ? `<a href='${href}'>${text}</a>` : undefined

export const augment = (params) => {


    return {

        ...params

        ,

        submission: params.asset        // little alias to clarify trasnlations where the asset points to instances.

        ,

        ...linksFrom(params),

        interpolation: {
            escapeValue: false
        }
    }
}

// returns a proxy of the t function that prefixes keys with a chain of fallbacks.
export const prefixed = (t, params, chain) => (keys, dflt = '') => {

    const keyArray = Array.isArray(keys) ? keys : [keys]
    const augmentedChain = [...chain, 'template']

    const finalKeys = [...keyArray.flatMap(key => augmentedChain.map(chainkey => `${chainkey}.${key}`)), ...keyArray]

    return t(finalKeys, dflt, params)

}


